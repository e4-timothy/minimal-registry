# e4-blitz-registry

This is a registry as described in https://doc.rust-lang.org/cargo/reference/running-a-registry.html:
> A minimal registry can be implemented by having a git repository that contains an index, and a server that contains the compressed .crate files created by cargo package. Users won’t be able to use Cargo to publish to it, but this may be sufficient for closed environments. The index format is described in Registry Index.